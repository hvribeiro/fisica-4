# Física 4

*Notas de aula do curso de Física 4 - DFI/UEM*

Vamos usar os livros:
- [**Curso de Física Básica: Eletromagnetismo. Vol. 3. Nussenzveig, Herch Moysés.**](http://93.174.95.29/main/5E5DC6B6ABE22DE9998530A0667656BD) 
- [**Curso de Física Básica: Ótica, Relatividade e Física Quântica. Vol. 4. Nussenzveig, Herch Moysés.**](http://libgen.gs/ads.php?md5=5C31BE82F1B7337587FE68FF9CE86721)

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/fisica_iv.pdf).


## Parte 1 - Circuitos Elétricos, Equações de Maxwell, Óptica Geométrica

- [Aula 01 - 18/01/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula01.png) | [Vídeo](https://drive.google.com/file/d/1_Ob1S2jdfYX5MhhGAjJYztrEo9KZRWyX/view?usp=sharing)
- [Aula 02 - 20/01/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula02.png) | [Vídeo](https://drive.google.com/file/d/1l8tBaD7wlJHXGSy-BTBTgYh3WQIuHuYI/view?usp=sharing)
- [Aula 03 - 25/01/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula03.png) | [Vídeo](https://drive.google.com/file/d/1AlwPYSl4iMhf7S0CHxA-3a3R_oQaJNOW/view?usp=sharing)
- [Aula 04 - 01/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula04.png) | [Vídeo](https://drive.google.com/file/d/1h9XUzSOJjcpgia5GJmzyGuRKJs-XHGwM/view?usp=sharing)
- [Aula 05 - 03/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula05.png) | [Vídeo](https://drive.google.com/file/d/1E6nq0qWJR6kVwQq02MjY-9HWhPIhXCt0/view?usp=sharing)
- [Aula 06 - 08/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula06.png) | [Vídeo](https://drive.google.com/file/d/10cncqF9eJvv8d0L0V-Z0dpCPQv0pF1sd/view?usp=sharing)
- [Aula 07 - 08/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula07.png) | [Vídeo](https://drive.google.com/file/d/1kIMr7QS2yncYz1-kJWreHB33o3fzqIGR/view?usp=sharing)
- [Aula 08 - 15/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula08.png) | [Vídeo](https://drive.google.com/file/d/1pdH5f_aR58ghqiziCU7n9VoeQUsN-F4T/view?usp=sharing)
- [Aula 09 - 17/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula09.png) | [Vídeo](https://drive.google.com/file/d/1wVClL826ZhSjovdV1Lq9QaVaUmliaBEt/view?usp=sharing)
- [Aula 10 - 22/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula10.png) | [Vídeo](https://drive.google.com/file/d/1BnuHUO5gc2zlCItlAVO9KAiHShmFKAWh/view?usp=sharing)
- [Aula 11 - 24/02/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte1/aula11.png) | [Vídeo](https://drive.google.com/file/d/1iHaTcWoDey8_3VLowppGT_N9VzHNhzXa/view?usp=sharing)

## Parte 2 - Interferência, Difração, Relatividade Restrita, Mecânica Quântica

- [Aula 12 - 01/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula12.png) | [Vídeo](https://drive.google.com/file/d/1Uo0kY7c5MDWfhgpTfjv1a63eKVPsjrun/view?usp=sharing)
- [Aula 13 - 03/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula13.png) | [Vídeo](https://drive.google.com/file/d/1pegcVrmXttLMjXAnSCpHAQ_t4VYyM4WM/view?usp=sharing)
- [Aula 14 - 08/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula14.png) | [Vídeo](https://drive.google.com/file/d/1E-IM-g09zccyRlRv4j9OvXMmCpEDN6z2/view?usp=sharing)
- [Aula 15 - 10/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula15.png) | [Vídeo](https://drive.google.com/file/d/1PXGbbCzbxWZE3LtTZrayTxBeJ6UDM3hP/view?usp=sharing)
- [Aula 16 - 15/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula16.png) | [Vídeo](https://drive.google.com/file/d/1dLKNNeXg0WrdBTv82q-ddHAQS-yzimTv/view?usp=sharing)
- [Aula 17 - 17/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula17.png) | [Vídeo](https://drive.google.com/file/d/1pCZ9H3B6C2IgAzzYQthFElTwIq9RyauQ/view?usp=sharing)
- [Aula 18 - 22/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula18.png) | [Vídeo](https://drive.google.com/file/d/1INeb2VTIBr84nBkyjeLgaacm6pbc1BUU/view?usp=sharing)
- [Aula 19 - 29/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula19.png) | [Vídeo](https://drive.google.com/file/d/1jGmHt384b7N2vhFcrUbmCeaXFhZmc_0S/view?usp=sharing)
- [Aula 20 - 31/03/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula20.png) | [Vídeo](https://drive.google.com/file/d/1fII2XxjlA63e0hsEiUexddMfliomzmpz/view?usp=sharing)
- [Aula 21 - 05/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula21.png) | [Vídeo](https://drive.google.com/file/d/1BgO3nNXXe46KCM8RCDV6KB8idY9KOb5g/view?usp=sharing)
- [Aula 22 - 07/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula22.png) | [Vídeo](https://drive.google.com/file/d/1lsra5gI1PTgqGnulGfhjDPNuxtDPlNI4/view?usp=sharing)
- [Aula 23 - 12/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula23.png) | [Vídeo](https://drive.google.com/file/d/1i8fJVk67r2sDop76ibq69EfoLjo5TbKe/view?usp=sharing)
- [Aula 24 - 14/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula24.png) | [Vídeo](https://drive.google.com/file/d/1DUd88exfrEdTrrSq3_TWbp6-5F23xRQl/view?usp=sharing)
- [Aula 25 - 19/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula25.png) | [Vídeo](https://drive.google.com/file/d/1S4oMGYFW0tWHP46J6aVn_ccf4yk4Q-Bl/view?usp=sharing)
- [Aula 26 - 26/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula26.png) | [Vídeo](https://drive.google.com/file/d/1h_nkKlp31wY71c5o4r4IAI63v_Fpfo__/view?usp=sharing)
- [Aula 27 - 28/04/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula27.png) | [Vídeo](https://drive.google.com/file/d/1Z6yf_52CDjZOuP1JOe93BpNPrTBwpGWJ/view?usp=sharing)
- [Aula 28 - 03/05/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula28.png) | [Vídeo](https://drive.google.com/file/d/17rduIpLt7OV_bBkGPvbkDKCi55sJ5xmB/view?usp=sharing)
- [Aula 29 - 05/05/2021](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/anos_anteriores/2020/parte2/aula29.png) | [Vídeo](https://drive.google.com/file/d/1GOjiBK46bffvb7eMoi8oC_WEKBgw9Bhv/view?usp=sharing)