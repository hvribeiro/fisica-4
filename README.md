# Física 4

*Notas de aula do curso de Física 4 - DFI/UEM*

Vamos usar os livros:
- [**Curso de Física Básica: Eletromagnetismo. Vol. 3. Nussenzveig, Herch Moysés.**](http://93.174.95.29/main/5E5DC6B6ABE22DE9998530A0667656BD) 
- [**Curso de Física Básica: Ótica, Relatividade e Física Quântica. Vol. 4. Nussenzveig, Herch Moysés.**](http://libgen.gs/ads.php?md5=5C31BE82F1B7337587FE68FF9CE86721)

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/fisica_iv.pdf).


## Parte 1 - Circuitos Elétricos, Equações de Maxwell, Óptica Geométrica

- [Aula 01 - 10/01/2022](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula01.png)
- [Aula 02 - 12/01/2022](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula02.png)
- [Aula 03 - 17/01/2022](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula03.png)
- [Aula 04 - 19/01/2022](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula04.png)
- [Aula 05 - 24/01/2022](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula05.png)
- [Aula 06 - 26/01/2022](https://gitlab.com/hvribeiro/fisica-4/-/blob/master/parte1/aula06.png)

## Parte 2 - Interferência, Difração, Relatividade Restrita, Mecânica Quântica